const getSum = (str1, str2) => {
  if (str1 !== {} && str1 !== [] && str2 !== {} && str2 !== []) {
    if (isNaN(str1) === true || isNaN(str2) === true) {
      return false
    } else {
      return String(+str1 + +str2);
    }
  }
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let count = 0;
  let countComments = 0;
  for (let item of listOfPosts) {
      if (item.author === authorName) {
          ++count;
      }
      if ('comments' in item) {
          for (let y = 0; y < item.comments.length; y++) {
            if(item.comments[y].author === authorName){
              ++countComments
            }
          }

      }

      

  };
  return `Post ${count}, comments ${countComments}`
  
};

const tickets = (people) => {
  let last = arr[arr.length - 1];

    let sum = 0;
    for (let y = 0; y < arr.length - 1; y++) {
        sum += arr[y];
    }
    if (last === sum) {
        return "YES";
    } else {
        return "NO";
    }
};


module.exports = { getSum, getQuantityPostsByAuthor, tickets };
